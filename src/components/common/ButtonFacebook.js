import React from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';

const img = require('../../../images/facebook.png');
const ButtonFacebook = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Image
        source={img}
        style={styles.imgStyle}
      />

      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  imgStyle: {
    marginTop: '1%',
    marginLeft: '20%',
    height: 40,
    width: 40
  },
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 20,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#007aff',
    margin: '2%',
    marginRight: '5%',
    borderRadius: 10

  }
};

export { ButtonFacebook };
