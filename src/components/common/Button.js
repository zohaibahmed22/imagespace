import React from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';

const img = require('../../../images/camera.png');
const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;

  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Image
        source={img}
        style={styles.imgStyle}
      />

      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  imgStyle: {
    marginTop: '1%',
    marginLeft: '25%',
    height: 50,
    width: 50
  },
  textStyle: {
    alignSelf: 'center',
    color: '#007aff',
    fontSize: 30,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: '5%'
  },
  buttonStyle: {
    flexDirection: 'row',
    flex: 1,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#007aff',
    marginTop: '1%',
    marginLeft: '2%',
    marginRight: '2%',
    marginBottom: '2%'

  }
};

export { Button };
