import React, {Component} from 'react';
import {Text, Image, View, ScrollView, Dimensions, CameraRoll} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {Row, Col, Grid} from 'react-native-easy-grid';
import {Button, Card, CardSection} from './common'; 

const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
};
const { height, width } = Dimensions.get('window');
class HomePage extends Component {
    state={
     images: [],
     isCameraLoaded: false,
     count: 0,
    }
    componentWillMount(){
        CameraRoll.getPhotos({first:1000}).then(
      (data) =>{
        const assets = data.edges;
        const images = assets.map((asset) => asset.node.image);
        this.setState({
          isCameraLoaded: true,
          images: images
        })
      },
      (error) => {
        console.warn(error);
      }
    );
    }
    onCameraButtonPress(){
      ImagePicker.launchCamera(options, (response) => {
          if (response.didCancel) {     }
          else if (response.error) {      }
          else if (response.customButton) {      }
          else {
            const source = { uri: response.uri };
            
           }
        });
    }
    insertSpace(count){
        if(count %3 ==0){
            return(
                <Text>
                    {'\n'}
                    {'\n'}
                    {'\n'}
                    {'\n'}
                    {'\n'}
                    {'\n'}
                </Text>
            );
        }
    }
    renderImages(){
        let count = 0;
       return( <View>
                { 
                    this.state.images.map((image,count) => (
                        <CardSection>
                        <Row style={{height: height * 0.5, width:width * 0.98}}>
                            <Image  
                                source={{ uri: image.uri }} 
                                style={{height: height * 0.5, width:width * 0.98}}
                            /> 
                            
                            </Row>
                            </CardSection>
                         )
                    )  
                    
                }
                
                
         </View>
         );
    }
    render(){
        return(
            <View>
               <Row style={{height:height * 0.1 , width:width }}>
                   <Button onPress={this.onCameraButtonPress.bind(this)}>
                        Camera
                   </Button>
                </Row>  
                <ScrollView>
                    {this.renderImages()}         
                </ScrollView>
            </View>

        );
    }
}

export default HomePage;