import React, {Component} from 'react';
import {AsyncStorage} from 'react-native';
import {Row, Col} from 'react-native-easy-grid';
import {Dimensions, Image, View} from 'react-native';
import {Actions} from 'react-native-router-flux';
import FBSDK,{ LoginManager } from 'react-native-fbsdk';
import {ButtonFacebook, Card,CardSection} from './common';

const { height, width } = Dimensions.get('window');
const img = require('../../images/LoginPageImage.jpg');
class LoginPage extends Component{

    state = {
        loginFlag: false
    };
     componentWillMount(){
       

     }
    onLoginPress(){
        LoginManager.logInWithReadPermissions(['public_profile']).then(function(result){
            if(result.isCancelled){
                console.log('canceledddd');
                
            }else{
                
                Actions.homePage({type: 'reset'});
                
            }
        },(function(error){
            console.log('Error');
        })
        )
        
    }
    render(){
        return (
            <Card>
           
            <Row style={{height: height* 0.75, width}}>
                <Image
                    source={img}
                    style={{height:height*0.75, width}}
                />
            </Row>
            
            <Row style={{height:height * 0.1 , width}}>
                <ButtonFacebook onPress={this.onLoginPress.bind(this)}>
                    Login With Facebook
                </ButtonFacebook>
            </Row>
           
           </Card>
        );
    }

}
const styles={
    viewStyles: {
        justifyContent: 'flex-start',
        flexDirection: 'row'
    }
};
export default LoginPage;