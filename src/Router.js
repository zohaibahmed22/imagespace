import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import HomePage from './components/HomePage';
import LoginPage from './components/LoginPage';

const RouterComponent = () => {
    return(
        <Router>
            <Scene key="homePage" 
               >
                
           <Scene
                key="loginPage"
                component={LoginPage}
                title="Image Space"
                
           />
           <Scene 
                key="homePage" 
                component={HomePage} 
                title="Home Page" 
                
                />
           
            </Scene>
        </Router>
    );
};

export default RouterComponent;