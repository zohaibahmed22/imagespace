import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import ImageSpace from './src/ImageSpace';

AppRegistry.registerComponent('ImageSpace', () => ImageSpace);
